/* CSC Objective
+### Please consider this a semi-open book exercise. Feel free to consult reference materials you use, but please don't try to google this exact problem and copy & paste a solution from stack overflow or any other site.
+
+
+Suppose you are tasked with syncing data from two different sources, a product database, and a storefront. The product database also contains the product taxonomy, i.e., product categories. Each category has a name, can have at most one parent category, and can have zero or more children. The product database provides three pieces of information about a category: the category’s id, it’s parent category’s id (if any), and the category’s name. The storefront, though, has one limitation: a child category cannot be inserted before its parent category has been created. Our job is to write a function that can take a JSON string of categories provided by the product database and sort them in the optimal insertion order for the storefront so that no category insertion will result in an integrity error. The taxonomy for categories can be arbitrarily deep. The return value should also be a JSON string.
+
+Your function should take a JSON object representing the categories from the product database and provide as output a list of dictionaries sorted in the proper insertion order. There may be more than one optimal ordering of the categories, but you only have to provide an optimal ordering. 
+
+You can assume:
+ - The input will always be solvable (there will be no missing parents)
+ - The input will always be valid JSON in the format of the example below, with no additional data
+ - There may be more than one root category (a category with no parents)
+
+This is a formatted JSON sample input, with one sample child (with both a parent and a child), one child with no children, and sample parent (with no parent)
+```json
+[
+  {
+    "name": "Accessories",
+    "id": 1,
+    "parent_id": 20,
+  },
+  {
+    "name": "Watches",
+    "id": 57,
+    "parent_id": 1
+  },
+  {
+    "name": "Men",
+    "id": 20,
+    "parent_id": null,
+  }
+]
+```
+
+This is a formatted JSON sample solution for the input above:
+```json
+[
+  {
+    "name": "Men",
+    "id": 20,
+    "parent_id": null
+  },
+  {
+    "name": "Accessories",
+    "id": 1,
+    "parent_id": 20
+  },
+  {
+    "name": "Watches",
+    "id": 57,
+    "parent_id": 1
+  }
+]
+```
+
+
+When submitting your solution, please provide it as a file containing the function or method which will perform the proper calculations, so we can run tests against it. Below is the general format of an acceptable submission in Javascript:
+
+```javascript
+module.exports = function sortCategoriesForInsert (inputJson) {
+  // Your code happens...
+  ///   ... which calculates properJsonOutput
+  return properJsonOutput
+}
+```
+
+Final notes on expected submissions: 
+ - Please use standard libraries where possible and limit the business logic to a one file submission.
+ - The order of key-value pairs within the JSON output does NOT matter.
+ - The whitespace of output JSON does NOT matter. 
+ - Your solution should take into account that there may be tens of thousands of categories. 
+
+
+If you have any questions, let me know. Looking forward to your answer!
*/
var categories;
var j = "";
var properJsonOutput;
var secondGroup;
var thirdGroup;
module.exports = function sortCategoriesForInsert (inputJson) {

/*
+Author: Ricardo Durán Naranjo
+Date: March 11 2021
+Version: 1.0
+Objective: Sort Categories For Insert
+Important: Use the basic libraries.
*/

categories = inputJson;
console.log("Categories to order:" + Object.keys(inputJson).length);
/*
The first step is to brake appart the problem.
1.- Get the Categories that won´t have touble to insert to the main file.(the ones that dosen´t have Parents).
*/
   properJsonOutput = inputJson.filter(function(category) {
	return category.parent_id === null;});
// 2.- Sort it by Category ID.	
    properJsonOutput.sort(function(a, b){return a.id - b.id});
// 3.- Get the second group of Categories that could give less trouble to insert to the main file.(Parent ID greater than th Category ID).	
   secondGroup = inputJson.filter(function(category) {
	return category.parent_id > category.id && category.id != null;});
// 4.- Sort it by Category ID.	
    secondGroup.sort(function(a, b){return a.id - b.id});
// 5.- Get the third group of Categories that could give more trouble to insert to the main file.(Category ID greater than the Parent ID).	
   thirdGroup = inputJson.filter(function(category) {
	return category.parent_id < category.id && category.parent_id != null;});
    thirdGroup.sort(function(a, b){return a.id - b.id});
// 6.- Sort it by Category ID.	
// 7.- Start working with the second group.	
    var vp;
    for (var i=0; i<secondGroup.length; i++){
         vp =0;
// 8.- Check if the Parent ID exists in the main file.	
           vp= verifyParent(secondGroup[i].parent_id) ;
           if (vp.length>0) { 
// 8a.- If it exists add the category to the main file from the Second Group.		   
              properJsonOutput = properJsonOutput.concat(secondGroup[i]);
// 8b.- Check if there are childs of the Category in the Third Group.
              filtered = verifyChild(secondGroup[i].id) ;
// 8a.- If childs exists add them to the main file from the Third Group.			  
           }else{
// 9.- If the Parent ID dosen´t exists in the main flie.
// 9a.- Check if the Parent ID exists in the Third Group.			   
            vp= verifythirdGroup(secondGroup[i].parent_id) ;
            if (vp.length>0) {
            properJsonOutput = properJsonOutput.concat(secondGroup[i]);
// 9b.- If it exists add the category to the main file from the Third group.
// 9c.- Check if there are childs of the Category in the Third Group.
// 9d.- If childs exists add them to the main file from the Third Group.
            }
           }
         }
 console.log("Categories ordered:" + Object.keys(properJsonOutput).length);
return properJsonOutput
}
// Check if the Parent ID exists in the main file.
function verifyParent(pt) {
	var v_Parent= properJsonOutput.filter(function(parent) {
	return parent.id === pt;});
 return v_Parent;
}

// Check if there are childs of the Category in the Third Group and add them to the main file.
function verifyChild(chd) {
	var v_Child= thirdGroup.filter(function(child) {
	return child.parent_id === chd;});
    for (var i=0; i<v_Child.length; i++){
           properJsonOutput = properJsonOutput.concat(v_Child[i]);}
 return v_Child;
}

/* Check if the Parent ID exists in the Third Group.
1.-If it exists add the category to the main file from the Third group.
2.- If it exists add the category to the main file from the Third group.
3.- Check if there are childs of the Category in the Third Group.
4.- If childs exists add them to the main file from the Third Group.
*/
function verifythirdGroup(pt) {
	var v_Parent= thirdGroup.filter(function(parent) {
	return parent.id === pt;});
    if (v_Parent.length>0) {
     for (var i=0; i<v_Parent.length; i++){
              properJsonOutput = properJsonOutput.concat(v_Parent[i]);
              filtered = verifyChild(v_Parent[i].id) ;
          }
           }
 return v_Parent;
}
